/**
 * Exemplo1: Programacao com threads
 * Autor: Paulo Henrique Bordignon
 * Ultima modificacao: 31/03/2017
 */
package atividade2;

import java.util.Random;
/**
 *
 * @author Paulo
 */
public class PrintTasks implements Runnable {

    private int id;
    
    private int contador;
    
    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();

    public PrintTasks(String name, int id){
        taskName = name;
        contador= 0;
        id = 0;
        //Tempo aleatorio entre 0 e 5 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
    }
    
    public void run(){
        try{
                if (this.id%2!=0){
                    System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                    Thread.sleep(sleepTime);
                }else{
                    System.out.printf("Tarefa: %s id par exibe valor %d ms\n", taskName, contador);
                    this.contador++;
                    Thread.yield();
                }
        
            //Estado de ESPERA SINCRONIZADA
            //Nesse ponto, a thread perde o processador, e permite que
            //outra thread execute
            Thread.sleep(sleepTime);
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
        
    }
}