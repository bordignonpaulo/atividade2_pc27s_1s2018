/**
 * Exemplo1: Programacao com threads
 * Autor: Paulo Henrique Bordignon
 * Ultima modificacao: 31/03/2018
 */
package atividade2;

import java.util.Random;
/**
 *
 * @author Paulo
 */
public class Exemplo2  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");
            
            
            for (int i=0; i<20; i++){
                Thread t = new Thread(new PrintTasks("thread1"+i,i));
                t.start();
            }
            
            System.out.println("Threads criadas");
        }
        
}
